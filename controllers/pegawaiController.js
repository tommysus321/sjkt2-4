// Membuat variabel pegawai dan mengimport/required dari model pegawai
const Pegawai = require("../models/Pegawai");
//const excel = require("exceljs");
// const ekspor = require("../routes")

// Dibawah ini kita menggunakan metod export, maka semua metod yang ada di dalam object(module.exports) akan ter export
module.exports = {
  // Membuat view untuk pegawai
    viewPegawai: async (req, res) => {
    try {
      // Membuat variabel pegawai, dan menunda eksekusi hingga proses async selesai lalu mengambil model pegawai
      // dan menggunakan method find untuk mengambil semua collection/tabel yang ada di database pegawai
      const pegawai = await Pegawai.find();
      // Membuat variabel untuk alertMessage  dan alertStatus
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
      // membuat variabel yang bersifat object dan memiliki sebuah pesan isinya mengambil dari variabel alertMessage dan alertStatus
      const alert = { message: alertMessage, status: alertStatus };
      /**
       * Lalu render viewnya yang ada di dalam file index
       * menampilkan datanya dan mendestracturkan nya, lalu memanggil variabel pegawai diatas
       * Lalu merender alert yang sudah di deklar di atas
       */
      res.render("index", {
        pegawai,
        alert,
       title: "SIJAKET", // Untuk title dari aplikasi kita, saya manamakannya dengan CRUD
     });
   } catch (error) {
      // Jika error maka akan meredirect ke route pegawai(routenya akan kita buat setelah selesai dengan pegawaiController)
    res.redirect("/pegawai");
   }
  },

  // Membuat create data untuk pegawai
  // Membuat fungsi untuk menambahkan data di form dan menggunakan async await
  addPegawai: async (req, res) => {
    // memberi validasi untuk inputan yang kosong
    try {
      // Membuat contanta untuk nama, nomor_wa, tanggal_pkt yang diambil dari body/yang diketikan di form
      const { nama, nomor_wa, tanggal_pkt } = req.body;
      // lalu mengembalikan fungsi dan membuat data dari scheme/model pegawai
      await Pegawai.create({ nama, nomor_wa, tanggal_pkt });
      // ketika create data berhasil memberikan notifikasi
      req.flash("alertMessage", "Success add data pegawai");
      req.flash("alertStatus", "success");
      res.redirect("/pegawai"); // Setelah berhasil membuat data akan meredirect ke tujuan yang sudah ditentukan
    } catch (error) {
      // ketika create data error memberikan notifikasi
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      // ketika inputan kosong, maka redirect kehalaman
      res.redirect("/pegawai");
    }
  },

  // Membuat update data untuk pegawai
  editPegawai: async (req, res) => {
    try {
      // Membuat variabel yang menerima id, dan nama yang didapat dari req body atau yang di inputkan di form input
      const { id, nama, nomor_wa, tanggal_pkt } = req.body;
      /*  mencari variabel yang dideklarasikan diatas dan mengecek _id yang ada di req body yang dikirim
   _id didapat database dan id isinya dari inputan user */
      const pegawai = await Pegawai.findOne({ _id: id });
      /* pegawai diambil dari fungsi diatas dan titik(.) nama diambil dari database = nama yang didapat dari req body
   yang tentu dikirimkan dari inputan user */
      pegawai.nama = nama;
      pegawai.nomor_wa = nomor_wa;
      pegawai.tanggal_pkt = tanggal_pkt;
      // Menyimpan datanya ke database
      await pegawai.save();
      // ketika edit data berhasill memberikan notifikasi/alert
      req.flash("alertMessage", "Success edit data pegawai");
      req.flash("alertStatus", "success");
      // Setelah berhasil maka meredirect ke tujuan yang ditentukan (/pegawai)
      res.redirect("/pegawai");
    } catch (error) {
      // ketika edit data error memberikan notifikasi erronya
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      // ketika inputan kosong maka redirect kehalaman (/pegawai)
      res.redirect("/pegawai");
    }
  },

  // Membuat delete data untuk pegawai
  deletePegawai: async (req, res) => {
    try {
      /*
  Membuat variabel yang menerima id yang didapat dari params
  id didapat database dan id isinya dari params
  */
      const { id } = req.params;
      // cek data pegawai yang mau di delete berdasarkan id
      const pegawai = await Pegawai.findOne({ _id: id });
      // setelah datanya sudah didapat maka menghapusnya
      await pegawai.remove();
      // ketika delete data memberikan notifikasi
      req.flash("alertMessage", "Success delete data pegawai");
      req.flash("alertStatus", "warning");
      // setelah berhasil remove maka melakukan redirect
      res.redirect("/pegawai");
    } catch (error) {
      // ketika create data error memberikan notifikasi
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      // ketika inputa kosong redirect kehalaman
      res.redirect("/pegawai");
    }
  },
}