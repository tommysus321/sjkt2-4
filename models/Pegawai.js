const mongoose = require("mongoose");

// Membuat variabel baru dengan nama pegawaiScheme
const pegawaiScheme = new mongoose.Schema({
  nama: {
    // Membuat type dari field nama yang berada di tabel pegawai bersifat string
    type: String,
    // maksud dari required adalah ketika data disimpan kedalam database, data tidak boleh kosong
    required: true,
  },
  nomor_wa: {
    // Membuat type dari field nama yang berada di tabel pegawai bersifat number
    type: String,
    required: true,
  },
  tanggal_pkt: {
    type: Date,
    required: true,
  },
});

module.exports = mongoose.model("Pegawai", pegawaiScheme);
